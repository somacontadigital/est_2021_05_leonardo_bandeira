# README #

Passos necessários para obter o projeto java com o teste de progração do processo de seleção a vaga de estágio backend da Somapay

### Como realizar o teste? ###

* Baixe esse repositório: 

```
	git clone https://bitbucket.org/somacontadigital/est_2021_05_leonardo_bandeira.git
```
	
* Abra-o na sua IDE preferida
* Acesse a classe Questao01.java, veja no comentário da classe o enunciado da questão, implemente o método proximaQuadradoPerfeito.
* Acesse a classe Questao02.java, veja no comentário da classe o enunciado da questão, implemente o método mascarar.
* Acesse a classe Questao03.java, veja no comentário da classe o enunciado da questão, implemente o método tribonacci.

### Como validar se as questões foram corretamente respondidas? ###

* Neste projeto existe uma classe de teste para cada questão
* Execute a classe de teste de cada questão e observe os resultados apresentados, se todos os resultados forem válidos, parabéns a questão está correta e pode ser submetida

### O que será avaliado nas respostas do candidato? ###

* Se as questões foram corretamente respondidas.
* Ae a resposta dada foi bem escrita, ou seja, se uma boa lógica foi utilizada
* Se foram bem utilizados os recursos oferecidos pela linguagem

### Prazo de entrega? ###

* Gostaríamos de receber as respostas até o às 23:59h do dia 28/05/2021. 