package br.com.somapay.selecao.estagio.a2021.m05.test;

import java.util.UUID;

import br.com.somapay.selecao.estagio.a2021.m05.Questao02;
import junit.framework.TestCase;

public class Questao02Test extends TestCase {

	public void test01() {
		assertEquals("################4321", new Questao02().mascarar("12345678900987654321"));
	}

	public void test02() {
		assertEquals("####3840", new Questao02().mascarar("60763840"));
	}

	public void test03() {
		assertEquals("5", new Questao02().mascarar("5"));
	}

	public void test04() {
		assertEquals("", new Questao02().mascarar(""));
	}

	public void testAleatorio() {
		for (int i = 1; i <= 100; i++) {
			String entrada = (UUID.randomUUID().toString()
					           + UUID.randomUUID().toString()
					           + UUID.randomUUID().toString()).substring(0, i) + "1234";
			String saida = repetir(i, "#") + "1234";
			assertEquals(saida, new Questao02().mascarar(entrada));
		}
	}

	private String repetir(int i, String string) {
		String retorno = "";
		for(int j = 0; j < i; j++) retorno += string;
		return retorno;
	}
}
