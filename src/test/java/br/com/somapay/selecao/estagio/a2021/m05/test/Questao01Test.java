package br.com.somapay.selecao.estagio.a2021.m05.test;

import br.com.somapay.selecao.estagio.a2021.m05.Questao01;
import junit.framework.TestCase;

public class Questao01Test extends TestCase
{

   public void test01()
   {
      assertEquals(Integer.valueOf(144), new Questao01().proximoQuadradoPerfeito(121)); 
   }
   
   public void test02()
   {
	   assertEquals(Integer.valueOf(16), new Questao01().proximoQuadradoPerfeito(9)); 
   }
   
   public void test03()
   {
	   assertEquals(Integer.valueOf(-1), new Questao01().proximoQuadradoPerfeito(101)); 
   }
   
   public void testAleatorio()
   {
	   for(int i = 1; i <= 100; i++) {
		   assertEquals(Integer.valueOf((i + 1) * (i + 1)), new Questao01().proximoQuadradoPerfeito(i * i));
		   assertEquals(Integer.valueOf(-1), new Questao01().proximoQuadradoPerfeito((i * i) + 1));
	   }
   }
}
