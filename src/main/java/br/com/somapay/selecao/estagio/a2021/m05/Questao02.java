package br.com.somapay.selecao.estagio.a2021.m05;

public class Questao02 {

	/**
	 * O número do cartão é uma informação muito sigilosa, pois se cair em mãos
	 * erradas pode ser utilizadas para realizar compras na internet, contra a
	 * vontade do seu dono. Por isso, os sistemas que trabalham com essa
	 * informação usam algoritmos para mascará-la. Você deve criar uma função
	 * para fazer isso, esta função deve exibir os últimos 4 caracteres do valor
	 * passado como parâmetro, todos os outros caracteres deve ser trocadas pelo
	 * caractere "#".
	 * 
	 * Exemplos: 
	 * 		mascarar("12345678900987654321") = "################4321"
	 *      mascarar("60763840") = "####3840"
	 *      mascarar("5") = "5"
	 *      mascarar("") = ""
	 * 
	 * @param dado
	 * @return
	 */
	public String mascarar(String dado) {
		// TODO adicionar a implementação aqui
		return "bl�";
	}

	public static void main(String[] args) {
		// TODO se quiser debugar ou fazer algum teste
	}
}
