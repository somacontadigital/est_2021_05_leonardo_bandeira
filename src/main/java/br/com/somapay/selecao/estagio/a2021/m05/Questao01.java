package br.com.somapay.selecao.estagio.a2021.m05;

public class Questao01 {

	/**
	 * Um número N é considerado um quadrado perfeito quando existe um número
	 * inteiro positivo X que ao ser multiplicado por ele mesmo tem como resultado
	 * o número N, ou seja X * X = N. 
	 * 
	 * Faça uma função que receba um número como
	 * parâmetro e caso esse número seja um quadrado perfeito, retorne o próximo
	 * quadrado perfeito, e caso não seja retorne -1
	 * 
	 * Exemplos:
	 *   proximoQuadradoPerfeito(121) = 144
     *   proximoQuadradoPerfeito(9) = 16
     *   proximoQuadradoPerfeito(101) = -1
     *   
	 * @param numero
	 * @return
	 */
	public Integer proximoQuadradoPerfeito(Integer numero) {
		//TODO adicionar a implementação aqui
		return 0;
	}

	
	public static void main(String[] args) {
		//TODO se quiser debugar ou fazer algum teste
	}
}
